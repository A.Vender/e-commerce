import React from 'react';
import { render, within } from '@testing-library/react';

import { Button } from './Button';

describe("Button", () => {
  const mockOnClick = jest.fn();

  test("Default buttons should be without class active and white", () => {
    const { getByTestId } = render(
      <Button onClick={mockOnClick()} />
    );

    const button = getByTestId("button");
    expect(button).not.toHaveClass("_active");
    expect(button).not.toHaveClass("_white");
  });

  test("Button with active props should have class active and white", () => {
    const { getByTestId } = render(
      <Button onClick={mockOnClick()} active white />
    );

    const button = getByTestId("button");
    expect(button).toHaveClass("_active");
    expect(button).toHaveClass("_white");
  });

  test("Button should render text inside component", () => {
    const { getByText } = render(
      <Button onClick={mockOnClick()} name="Click me" />
    );
    expect(getByText("Click me")).toBeInTheDocument();
  });

  test("Button should render children", () => {
    const { getByTestId } = render(
      <Button onClick={mockOnClick()}>
        <span>Button children</span>
      </Button>
    );
    const button = getByTestId("button");
    const { getByText } = within(button);
    expect(getByText("Button children")).toBeInTheDocument();
  });
});