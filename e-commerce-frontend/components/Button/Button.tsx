import React from 'react';
import classnames from 'classnames';

import { ButtonTypes } from './types';

import styles from './styles.module.scss';

export const Button: React.FC<ButtonTypes> = ({
  dataTestId,
  disabled,
  white,
  className,
  active,
  name,
  onClick,
  children
}) => (
  <button
    data-testid={dataTestId || "button"}
    className={classnames(styles.button, className, {
      [styles._active]: active,
      [styles._white]: white,
      [styles._disabled]: disabled,
    })}
    onClick={onClick}
  >
    {name && name}
    {children && children}
  </button>
);