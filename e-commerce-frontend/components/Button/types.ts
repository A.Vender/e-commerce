import React from 'react';

export interface ButtonTypes {
  /** Test id for rtl */
  dataTestId?: string;
  /** Disabled button or not disabled */
  disabled?: boolean;
  /** Change color from black to white */
  white?: boolean;
  /** Button text */
  name?: string;
  /** Extra className */
  className?: string;
  /** Children inside button, for example Icon */
  children?: React.ReactElement;
  /** Button click event */
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
  /** Is button active. Button change color if button active */
  active?: boolean;
}