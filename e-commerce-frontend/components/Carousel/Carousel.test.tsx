import React from 'react';
import { render } from '@testing-library/react';
import userEvent from "@testing-library/user-event";

import { Carousel } from './Carousel';

describe("Carousel", () => {
  test("Carousel should has two buttons", () => {
    const { getAllByRole } = render(
      <Carousel>
        <div>slide #1</div>
        <div>slide #2</div>
        <div>slide #3</div>
      </Carousel>
    );

    const buttons = getAllByRole('button');
    expect(buttons).toHaveLength(2);
  });

  test("Left button should has class _disabled", () => {
    const { getByTestId } = render(
      <Carousel>
        <div>slide #1</div>
        <div>slide #2</div>
        <div>slide #3</div>
      </Carousel>
    );

    const button = getByTestId("leftButton");
    expect(button).toHaveClass("_disabled");
  });

  test("Carousel should has style transform: translateX(-0%) by default", () => {
    const { getByTestId } = render(
      <Carousel>
        <div>slide #1</div>
      </Carousel>
    );

    expect(getByTestId("carouselWrapperList")).toHaveStyle("transform: translateX(-0%);");
  });

  test("Transform style should change after button click", async () => {
    const user = userEvent.setup();

    const { getByTestId, container } = render(
      <Carousel>
        <div>slide #1</div>
        <div>slide #2</div>
        <div>slide #3</div>
      </Carousel>
    );

    const rightButton = getByTestId("rightButton");
    await user.click(rightButton);

    expect(getByTestId("carouselWrapperList")).toHaveStyle("transform: translateX(-50%);");
  });
});
