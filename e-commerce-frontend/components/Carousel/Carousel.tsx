import React, { useState, useCallback, useEffect } from 'react';
import classnames from 'classnames';

import { Button } from '../Button';
import { ChevronRightIcon, ChevronLeftIcon } from '../icons';
import { Wrapper } from './Wrapper';
import { Container } from './Container';

import {
  CAROUSEL_ITEM_STEP,
  CAROUSEL_TRANSFORM_STEP
} from '../../utils/constants';
import { CarouselTypes } from './types';

import styles from './styles.module.scss';

export const Carousel: React.FC<CarouselTypes> = ({
  children
}) => {
  const [currentIndex, setCurrentIndex] = useState(0)
  const [length, setLength] = useState(React.Children.count(children));

  useEffect(() => {
    setLength(React.Children.count(children));
  }, [children]);

  // Move carousel to the right;
  const next = useCallback(() => setCurrentIndex(prevState => {
    let index = currentIndex;
    if (currentIndex < (length - CAROUSEL_ITEM_STEP)) index = prevState + 1;
    return index;
  }), []);

  // Move carousel to the left;
  const prev = useCallback(() => setCurrentIndex(prevState => prevState - 1), []);

  return (
    <Container>
      <Button
        white
        dataTestId="leftButton"
        disabled={!(currentIndex > 0)}
        onClick={prev}
        className={classnames(
          styles.arrow,
          styles._left
        )}
      >
        <ChevronLeftIcon />
      </Button>
      <Wrapper
        transform={`translateX(-${currentIndex * CAROUSEL_TRANSFORM_STEP}%)`}
      >
        {children}
      </Wrapper>
      <Button
        white
        dataTestId="rightButton"
        disabled={!(currentIndex < (length - CAROUSEL_ITEM_STEP) )}
        onClick={next}
        className={classnames(
          styles.arrow,
          styles._right
        )}
      >
        <ChevronRightIcon />
      </Button>
    </Container>
  );
}