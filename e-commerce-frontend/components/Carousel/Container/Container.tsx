import React from "react";

import { CarouselContainerTypes } from './types';

import styles from "./styles.module.scss";

export const Container: React.FC<CarouselContainerTypes> = ({
  children,
}) => (
  <div
    className={styles.carousel}
    data-testid="carousel"
  >
    {children}
  </div>
);