import React from "react";

export interface CarouselContainerTypes {
  /** Carousel children inside container */
  children: React.ReactNode;
}