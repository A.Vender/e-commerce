import React from 'react';

import { CarouselWrapperTypes } from './types';

import styles from "./styles.module.scss";

export const Wrapper: React.FC<CarouselWrapperTypes> = ({
  transform,
  children,
}) => (
  <div className={styles.wrapper}>
    <div className={styles.wrapper_content}>
      <div
        data-testid="carouselWrapperList"
        className={styles.wrapper_list}
        style={{ transform }}
      >
        {children}
      </div>
    </div>
  </div>
);