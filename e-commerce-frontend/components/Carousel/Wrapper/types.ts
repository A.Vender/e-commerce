import React from "react";

export interface CarouselWrapperTypes {
  /** Step for carousel moving */
  transform: string;
  /** Children nodes inside carousel */
  children: React.ReactNode;
}