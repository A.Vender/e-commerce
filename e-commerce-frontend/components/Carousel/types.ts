import React from 'react';

export interface CarouselTypes {
  /** Children nodes */
  children?: React.ReactElement | React.ReactElement[];
}