import React from 'react';

import { ContainerTypes } from './types';

import styles from './styles.module.scss';

export const Container: React.FC<ContainerTypes> = ({
  children,
}) => (
  <div className={styles.container}>
    {children}
  </div>
);

