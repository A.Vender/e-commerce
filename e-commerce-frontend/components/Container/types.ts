import React from 'react';

export interface ContainerTypes {
  /** Nodes inside container */
  children: React.ReactElement;
}