import React, { memo } from "react";

import styles from './styles.module.scss';

export const Logo = memo(() => (
  <a
    className={styles.logo}
    href={"/"}
  >
    <img
      src={"/images/logo.png"}
      alt="planet express"
    />
  </a>
));