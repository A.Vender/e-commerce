import React from 'react';
import { useSelector } from 'react-redux';

import { selectBasketList } from '../../../store/basket';
import { BASKET_BUTTON_NAME, BASKET_URL } from '../../../utils/constants';

import { BasketIcon } from '../../icons';

import styles from './styles.module.scss';

export const Basket = () => {
  const reduxState = useSelector(selectBasketList);
  return (
    <a href={BASKET_URL}
       className={styles.basket}
    >
      {reduxState.basket && (reduxState.basket.length > 0) && (
        <span className={styles.basket_counter}>
          {reduxState.basket.length}
        </span>
      )}
      {BASKET_BUTTON_NAME}
      <BasketIcon />
    </a>
  );
}