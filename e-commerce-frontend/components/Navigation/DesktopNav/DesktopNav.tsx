import React from 'react';
import classnames from 'classnames';

import { Logo } from '../../Logo';
import { Basket } from '../Basket';
import { NavLinks } from '../NavLinks';

import { DesktopNavTypes } from './types';

import styles from './styles.module.scss';

export const DesktopNav: React.FC<DesktopNavTypes> = ({
  className,
}) => (
  <div className={classnames(styles.desktop_nav, className)}>
    <Logo />
    <NavLinks />
    <Basket />
  </div>
);