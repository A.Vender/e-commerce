export type DesktopNavTypes = {
  /** Nav class name useful for @media */
  className: string;
}