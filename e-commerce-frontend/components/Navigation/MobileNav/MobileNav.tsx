import React, { useState, useCallback } from 'react';
import classnames from 'classnames';

import { Logo } from '../../Logo';
import { Button } from '../../Button';
import { LeftSideAlignmentIcon } from '../../icons';
import { NavModal } from '../NavModal';

import { MobileNavTypes } from "./types";

import styles from './styles.module.scss';

export const MobileNav: React.FC<MobileNavTypes> = ({
  className,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const isOpenModalClick = useCallback(() => setIsOpen((isOpen) => !isOpen), []);

  return (
    <div className={classnames(styles.mobile_nav, className)}>
      <Logo />
      <Button
        white
        className={styles.mobile_burger}
        onClick={isOpenModalClick}
      >
        <LeftSideAlignmentIcon />
      </Button>

      {isOpen && (
        <NavModal onClick={isOpenModalClick} />
      )}
    </div>
  );
}