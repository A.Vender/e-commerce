export type MobileNavTypes = {
  /** Nav class name useful for @media */
  className: string;
}