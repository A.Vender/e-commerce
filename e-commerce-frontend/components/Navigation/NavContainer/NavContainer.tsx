import React from 'react';

import { NavContainerTypes } from './types';

import styles from './styles.module.scss';

export const NavContainer: React.FC<NavContainerTypes> = ({
  children
}) => (
  <div className={styles.nav_container}>
    <nav className={styles.nav_wrapper}>
      {children}
    </nav>
  </div>
);