import React from 'react';

export type NavContainerTypes = {
  /** All navigation nodes */
  children: React.ReactElement;
}