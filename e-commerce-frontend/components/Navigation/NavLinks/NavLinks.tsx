import React, { memo } from 'react';

import { AUTH_NAVIGATION } from '../../../utils/constants';

import styles from "./styles.module.scss";

export const NavLinks = memo(() => (
  <ul className={styles.nav_links}>
    {AUTH_NAVIGATION.map(item => (
      <li key={item.name}>
        <a
          className={styles.nav_link}
          href={item.url}
        >
          {item.name}
        </a>
      </li>
    ))}
  </ul>
));