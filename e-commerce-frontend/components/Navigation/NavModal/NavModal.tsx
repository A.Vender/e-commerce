import React from 'react';

import { Logo } from '../../Logo';
import { Button } from '../../Button';
import { CloseIcon } from '../../icons';
import { NavLinks } from '../NavLinks';
import { Basket } from '../Basket';

import { NavModalTypes } from './types';

import styles from './styles.module.scss';

export const NavModal: React.FC<NavModalTypes> = ({
  onClick
}) => (
  <div className={styles.modal}>
    <div className={styles.modal_wrapper}>
      <div className={styles.modal_topbar}>
        <Logo />
        <Button
          white
          className={styles.modal_close}
          onClick={onClick}
        >
          <CloseIcon />
        </Button>
      </div>

      <NavLinks />

      <Basket />
    </div>
  </div>
);