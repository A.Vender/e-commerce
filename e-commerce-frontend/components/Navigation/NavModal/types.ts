import React from 'react';

export type NavModalTypes = {
  /** event for close mobile navigation modal */
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}