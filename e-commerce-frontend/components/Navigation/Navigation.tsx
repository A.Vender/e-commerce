import React, { useEffect } from 'react';
import { parseCookies } from 'nookies';
import { useDispatch } from 'react-redux';

import { setBasket } from '../../store/store';

import { NavContainer } from './NavContainer';
import { DesktopNav } from './DesktopNav';
import { MobileNav } from './MobileNav';

import styles from './styles.module.scss';

export const Navigation = () => {
  const dispatch = useDispatch();

  // Get basket from cookie and add to the store;
  useEffect(() => {
    const cookies = parseCookies();
    if (cookies.basket) {
      dispatch(setBasket(JSON.parse(cookies.basket)));
    }
  }, [])

  return (
    <NavContainer>
      <React.Fragment>
        <DesktopNav className={styles.navigation_desktop} />
        <MobileNav className={styles.navigation_mobile} />
      </React.Fragment>
    </NavContainer>
  );
}