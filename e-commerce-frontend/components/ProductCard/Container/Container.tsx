import React from 'react';

import { ProductCardContainerTypes } from './types';

import styles from './styles.module.scss';

export const Container: React.FC<ProductCardContainerTypes> = ({
  children,
}) => (
  <div className={styles.product}>
    {children}
  </div>
);