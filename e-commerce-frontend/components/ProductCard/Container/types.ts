import React from 'react';

export interface ProductCardContainerTypes {
  /** Nodes inside container */
  children: React.ReactElement | React.ReactElement[];
}