import React from 'react';

import { Button } from '../../Button';

import { ProductCardTypes } from './types';

import styles from './styles.module.scss';

export const Info: React.FC<ProductCardTypes> = ({
  isActive,
  onClick,
  id,
  title,
  description,
  price,
  rating,
}) => (
  <div className={styles.product}>
    <div className={styles.product_wrapper}>
      <a href={`product/${id}`} />
      <div className={styles.product_title}>{title}</div>

      {description && (
        <div className={styles.product_description}>{description}</div>
      )}

      {rating && (
        <div className={styles.product_rating}>{`${rating}/10`}</div>
      )}

      <div className={styles.product_price}>{`Price: $${price}`}</div>
    </div>

    <Button
      active={isActive}
      name={isActive ? "Remove from basket" : "Add to basket"}
      onClick={onClick}
    />
  </div>
);