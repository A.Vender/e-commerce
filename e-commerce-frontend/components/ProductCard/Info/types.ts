import React from 'react';

export interface ProductCardTypes {
  /** Product id */
  id: number;
  /** Product title */
  title: string;
  /** Product description */
  description?: string;
  /** Product price */
  price: number;
  /** Product rating */
  rating?: number;
  /** Is product button active */
  isActive?: boolean;
  /** Add/remove product to/from basket */
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}