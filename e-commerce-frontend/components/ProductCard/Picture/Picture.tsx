import React from 'react';

import { ProductCardPictureTypes } from './types';
import { API_URL } from '../../../utils/constants';

import styles from './styles.module.scss';

export const Picture: React.FC<ProductCardPictureTypes> = ({
  src,
  title,
}) => (
  <div className={styles.picture}>
    <img
      src={`${API_URL}${src}`}
      alt={title}
    />
  </div>
);