export interface ProductCardPictureTypes {
  /** Product picture */
  src: string;
  /** Product title for alt */
  title: string;
}