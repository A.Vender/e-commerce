import React, {useMemo} from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Info } from './Info';
import { Container } from './Container';
import { Picture } from './Picture';

import { addItemToBasket } from '../../store/store';
import { selectBasketList } from '../../store/basket';

import { ProductCardTypes } from './types';

export const ProductCard: React.FC<ProductCardTypes> = ({
  id,
  title,
  description,
  price,
  rating,
  thumb
}) => {
  const dispatch = useDispatch();
  const reduxState = useSelector(selectBasketList);

  // Check is basket include product id;
  const isActive = useMemo(() =>
    reduxState.basket.includes(id),
    [reduxState.basket]
  );

  // Add new id to basket;
  const handleAddToBasket = () => {
    dispatch(addItemToBasket(id));
  };

  return (
    <Container>
      <Picture
        src={thumb}
        title={title}
      />
      <Info
        id={id}
        title={title}
        description={description}
        price={price}
        rating={rating}
        isActive={isActive}
        onClick={handleAddToBasket}
      />
    </Container>
  );
}