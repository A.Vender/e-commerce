export interface ProductCardTypes {
  /** Product id */
  id: number;
  /** Product name */
  title: string;
  /** Product description */
  description: string;
  /** Product price */
  price: number;
  /** Product rating */
  rating: number;
  /** Product picture */
  thumb: string;
}