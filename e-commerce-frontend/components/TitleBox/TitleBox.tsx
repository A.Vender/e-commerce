import React from 'react';

import { TitleBoxTypes } from './types';

import styles from './styles.module.scss';

export const TitleBox: React.FC<TitleBoxTypes> = ({
  children,
  title,
}) => (
  <div className={styles.box}>
    {title && (
      <h2 className={styles.box_title}>{title}</h2>
    )}
    {children}
  </div>
);

