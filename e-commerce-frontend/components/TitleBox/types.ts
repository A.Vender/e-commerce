import React from 'react';

export interface TitleBoxTypes {
  /** Nodes inside container */
  children: React.ReactElement;
  /** Title for container */
  title?: string;
}