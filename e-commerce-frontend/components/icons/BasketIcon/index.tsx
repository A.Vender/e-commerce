import { withSvgWrap } from '../SvgWrap';

export const BasketIcon = withSvgWrap(
  'M15.4 3.4 18.6 9H22v2h-1.2l-.7 9c0 .6-.5 1-1 1H4.9a1 1 0 0 1-1-1l-.7-9H2V9h3.4l3.2-5.6 1.8 1L7.7 9h8.6l-2.7-4.6 1.8-1Zm3.4 7.6H5.2l.6 8h12.4l.6-8ZM13 17v-4h-2v4h2Zm-4-4v4H7v-4h2Zm8 4v-4h-2v4h2Z'
);