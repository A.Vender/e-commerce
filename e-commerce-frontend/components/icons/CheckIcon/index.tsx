import { withSvgWrap } from '../SvgWrap';

export const CheckIcon = withSvgWrap(
  'm10.6 16 9.7-9.7 1.5 1.5-11.2 11.3-6.8-6.8 1.5-1.5 5.3 5.3Z'
);