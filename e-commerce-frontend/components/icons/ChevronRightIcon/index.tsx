import { withSvgWrap } from '../SvgWrap';

export const ChevronRightIcon = withSvgWrap(
  'M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z'
);