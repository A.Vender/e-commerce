import { withSvgWrap } from '../SvgWrap';

export const CloseIcon = withSvgWrap(
  'm12 10.6 5-5L18.4 7l-5 5 5 5-1.4 1.4-5-5-5 5L5.7 17l5-5-5-5 1.5-1.4 4.9 5Z'
);