import { withSvgWrap } from '../SvgWrap';

export const LeftSideAlignmentIcon = withSvgWrap(
  'M3 4h18v2H3V4Zm0 7h12v2H3v-2Zm18 7H3v2h18v-2Z'
);