import React, { memo, ReactElement } from 'react';

import { IconProps, SvgWrapProps } from './types';

const defaultViewBox = '0 0 24 24';

export const SvgWrap = ({
  size = 24,
  fill = '#222222',
  viewBox = defaultViewBox,
  ratio = 1,
  children,
}: IconProps & SvgWrapProps & { children: ReactElement }): ReactElement => (
  <svg
    fill={fill}
    height={size}
    viewBox={viewBox}
    width={size * ratio}
    xmlns="http://www.w3.org/2000/svg"
    fillRule="evenodd"
    clipRule="evenodd"
  >
    {children}
  </svg>
);

const getRatioFromViewBox = (viewBox: string): number => {
  const [, , width, height] = viewBox.split(' ');
  return +width / +height;
};

export function withSvgWrap<
  T extends React.ComponentType<IconProps>,
  M extends React.MemoExoticComponent<T>,
>(
  iconBody: ReactElement | string,
  { viewBox = defaultViewBox, ratio }: SvgWrapProps = {},
): M {
  const targetRatio = ratio || getRatioFromViewBox(viewBox);
  const SvgIcon = (props: React.ComponentProps<T>) => (
    <SvgWrap
      ratio={targetRatio}
      viewBox={viewBox}
      {...props}
    >
      {typeof iconBody === 'string' ? <path d={iconBody} /> : iconBody}
    </SvgWrap>
  );

  return memo(SvgIcon) as M;
}
