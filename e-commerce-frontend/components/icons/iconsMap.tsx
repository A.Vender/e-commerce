import React, { ReactElement } from 'react';
import classnames from 'classnames/bind';

import { IconProps } from './types';
import * as Icons from './index';

import styles from './styles.module.scss';

const cn = classnames.bind(styles);

type IconItem = { name: string; component: ReactElement };

export const getBasicIcons = ({ fill, size }: IconProps): IconItem[] => [
  {
    name: 'left side alignment',
    component: <Icons.LeftSideAlignmentIcon fill={fill} size={size} />,
  },
  {
    name: 'chevron right',
    component: <Icons.ChevronRightIcon fill={fill} size={size} />,
  },
  {
    name: 'chevron left',
    component: <Icons.ChevronLeftIcon fill={fill} size={size} />,
  },
  {
    name: 'close',
    component: <Icons.CloseIcon fill={fill} size={size} />,
  },
  {
    name: 'check',
    component: <Icons.CheckIcon fill={fill} size={size} />,
  },

  {
    name: 'basket',
    component: <Icons.BasketIcon fill={fill} size={size} />,
  },
];