export type IconProps = {
    /** icon size */
    size?: number;
    /** icon color */
    fill?: string;
  };
  
  export type SvgWrapProps = {
    /** Custom viewbox for brand icons */
    viewBox?: string;
    /** Custom ration for brand icons */
    ratio?: number;
  };