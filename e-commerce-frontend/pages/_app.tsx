import type { AppProps } from 'next/app';
import { Provider } from "react-redux";

import { Navigation, Container } from '../components';

import store from '../store/store';

import '../styles/globals.scss';
import '../styles/fonts.scss';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Navigation />
      <Container>
        <Component {...pageProps} />
      </Container>
    </Provider>
  )
}
