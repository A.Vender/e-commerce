import { NextApiRequest } from 'next';
import { API_URL } from '../../utils/constants';

export const getBasketPageData = async (req: NextApiRequest) => {
  let allDataResult = null;
  const cookieBasket = req.cookies.basket;
  const basket = await JSON.parse(cookieBasket as string);

  try {
    const fetchArray = basket.map((item: number) =>
      fetch(`${API_URL}/api/products/${item}?populate=*`).then((res) => res.json())
    );
    const allData = Promise.all(fetchArray);
    allDataResult = await allData.then((res) => res);
  } catch (err) {
    console.log('BASKET ERROR: ', err);
  }

  return allDataResult;
}