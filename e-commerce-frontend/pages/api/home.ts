import { API_URL } from '../../utils/constants';

export const getHomePageData = async () => {
  let resData = null;

  try {
    const res = await fetch(`${API_URL}/api/cuisines?populate=*`);
    resData = await res.json();
  } catch (err) {
    console.log('BASE PAGE ERROR ... ', err);
  }

  return resData?.data;
}
