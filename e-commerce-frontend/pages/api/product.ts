import { API_URL } from '../../utils/constants';

type QueryType = string | string[] | undefined;

export const getProductPageData = async (queryId: QueryType) => {
  let resData = null;

  try {
    const res = await fetch(`${API_URL}/api/products/${queryId}?populate=*`);
    resData = await res.json();
  } catch (err) {
    console.log('PRODUCT PAGE ERROR ... ', err);
  }

  return resData?.data;
}
