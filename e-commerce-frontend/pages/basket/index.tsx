import React from 'react';
import { NextPageContext, NextApiRequest } from 'next';

import {
  Carousel,
  ProductCard
} from '../../components';

import { getBasketPageData } from '../api/basket';

import { BasketTypes } from './types';

const Basket = ({ data }: BasketTypes) => {
  return (
    <Carousel>
      {data.map(({
        data: {
          id,
          attributes: {
            title,
            description,
            price,
            rating,
            thumb,
          }
        }
      }) => (
        <ProductCard
          id={id}
          key={id}
          title={title}
          description={description}
          price={price}
          rating={rating}
          thumb={thumb}
        />
      ))}
    </Carousel>
  )
}

Basket.getInitialProps = async (ctx: NextPageContext) => {
  const data = await getBasketPageData(ctx?.req as NextApiRequest);
  return { data }
}

export default Basket;