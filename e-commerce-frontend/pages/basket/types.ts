import { ProductAttributes } from '../types'

interface BasketItem {
  data: {
    /** product id */
    id: number,
    /** product attributes */
    attributes: ProductAttributes;
  }
}

export interface BasketTypes {
  data: BasketItem[];
}