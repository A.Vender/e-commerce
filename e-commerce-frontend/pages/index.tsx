import React from 'react';

import {
  TitleBox,
  Carousel,
  ProductCard,
} from '../components';

import { HomePageData } from './types';

import { getHomePageData } from './api/home';

export default function Home({ data }: HomePageData) {
  return (
    <React.Fragment>
      {data.map(({
        id: pid,
        attributes: {
         products,
         title: productTitle
        }
      }) => {
        return (
          <TitleBox
            key={pid}
            title={productTitle}
          >
            <Carousel>
              {products.data.map(({
                id,
                attributes: {
                  title,
                  description,
                  price,
                  rating,
                  thumb,
                }}) => {

                return (
                  <ProductCard
                    id={id}
                    key={id}
                    title={title}
                    description={description}
                    price={price}
                    rating={rating}
                    thumb={thumb}
                  />
                )
              })}
            </Carousel>
          </TitleBox>
        )
      })}
    </React.Fragment>
  )
}

Home.getInitialProps = async () => {
  const data = await getHomePageData();
  return {
    data,
  }
}