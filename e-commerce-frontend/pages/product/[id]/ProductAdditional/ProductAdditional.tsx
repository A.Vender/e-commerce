import React from 'react';

import { ProductAdditionalTypes } from './types';

import styles from './styles.module.scss';

export const ProductAdditional: React.FC<ProductAdditionalTypes> = ({
  additional,
}) => (
  <div className={styles.additional}>{additional}</div>
);