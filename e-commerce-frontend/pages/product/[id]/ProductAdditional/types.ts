export interface ProductAdditionalTypes {
  /** Product addition information */
  additional: string;
}