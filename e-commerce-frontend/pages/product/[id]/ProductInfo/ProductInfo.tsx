import React from 'react';

import { Button } from "../../../../components";
import { ProductInfoTypes } from './types';

import styles from './styles.module.scss';

export const ProductInfo: React.FC<ProductInfoTypes> = ({
  onClick,
  isActive,
  description,
  title,
  rating,
  price
}) => (
  <div className={styles.info}>
    <h2 className={styles.info_title}>{title}</h2>

    {description && (
      <div className={styles.info_description}>{description}</div>
    )}

    {rating && (
      <div className={styles.info_rating}>{`${rating}/10`}</div>
    )}

    <div className={styles.info_price}>{`Price: $${price}`}</div>
    <Button
      active={isActive}
      name={isActive ? "Remove from basket" : "Add to basket"}
      onClick={onClick}
    />
  </div>
);