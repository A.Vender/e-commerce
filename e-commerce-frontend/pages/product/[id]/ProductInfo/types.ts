import React from "react";

export interface ProductInfoTypes {
  /** Product title */
  title: string;
  /** Product title */
  description?: string;
  /** Product rating */
  rating?: number;
  /** Product price */
  price: number;
  /** Is product button active */
  isActive?: boolean;
  /** Add/remove product to/from basket */
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}