import React from 'react';

import { PictureTypes } from './types';
import { API_URL } from '../../../../utils/constants';

import styles from './styles.module.scss';

export const ProductPicture: React.FC<PictureTypes> = ({
  src,
  title,
}) => (
  <div className={styles.picture}>
    <img
      src={`${API_URL}${src}`}
      alt={title}
    />
  </div>
);