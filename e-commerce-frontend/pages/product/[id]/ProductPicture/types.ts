export interface PictureTypes {
  /** Product picture */
  src: string;
  /** Product title for alt */
  title: string;
}