import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NextPageContext } from 'next';

import { ProductAdditional } from './ProductAdditional';
import { ProductPicture } from './ProductPicture';
import { ProductInfo } from './ProductInfo';

import { addItemToBasket } from "../../../store/store";
import { selectBasketList } from "../../../store/basket";
import { getProductPageData } from '../../api/product';

import { ProductPage } from './types';

import styles from './styles.module.scss';

const Product = ({ data: {
  id,
  attributes: {
    title,
    thumb,
    description,
    price,
    rating,
    additional
  }
}}: ProductPage) => {
  const dispatch = useDispatch();
  const reduxState = useSelector(selectBasketList);

  // Check is basket include product id;
  const isActive = useMemo(() =>
    reduxState.basket.includes(id),
    [reduxState.basket]
  );

  // Add item to the basket;
  const handleAddToBasket = () => {
    dispatch(addItemToBasket(id));
  };

  return (
    <div className={styles.product}>
      <div className={styles.product_wrapper}>
        <ProductPicture
          src={thumb}
          title={title}
        />
        <ProductInfo
          title={title}
          description={description}
          price={price}
          rating={rating}
          isActive={isActive}
          onClick={handleAddToBasket}
        />
      </div>

      <ProductAdditional
        additional={additional}
      />
    </div>
  )
}

Product.getInitialProps = async (ctx: NextPageContext) => {
  const data = await getProductPageData(ctx.query.id);
  return { data }
}

export default Product;