import { Product } from '../../types';

export interface ProductPage {
  /** Current Product data */
  data: Product;
}