export interface ProductAttributes {
  /** Product title */
  title: string;
  /** Product description */
  description: string;
  /** Product alias */
  pid: string;
  /** Product price */
  price: number;
  /** Product rating */
  rating: number;
  /** Product preview image url */
  thumb: string;
  /** Additional information about product */
  additional: string;
}

export interface Product {
  /** Product id */
  id: number;
  /** Product attributes */
  attributes: ProductAttributes;
}

interface CuisinesAttributes {
  /** Cuisine title */
  title: string;
  products: {
    data: Product[];
  }
}

interface Cuisines {
  /** Cuisine id */
  id: number;
  /** Cuisine attributes */
  attributes: CuisinesAttributes;
}

export interface HomePageData {
  data: Cuisines[];
}