import { createAction } from "@reduxjs/toolkit";

/* Selectors */
const selectBasketState = (rootState: { basket: { basket: number[] }}) => rootState.basket;
export const selectBasketList = (rootState: { basket: { basket: number[] }}) =>
  selectBasketState(rootState);

/* Actions */
export const addItemToBasket = createAction("addBasket", (productId) => {
  return { payload: { id: productId } };
});
export const setBasket = createAction("setBasket", (basket) => {
  return { payload: basket };
});