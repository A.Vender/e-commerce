import { setCookie } from 'nookies';
import { configureStore, createSlice } from '@reduxjs/toolkit';

/* Initial state */
const initialState = {
  basket: []
}

const basketSlice = createSlice({
  name: 'basket',
  initialState,
  reducers: {
    setBasket: (state, action) => {
      state.basket = action.payload;
    },
    addItemToBasket: (state, action) => {
      let newState = null;
      // @ts-ignore
      if (state.basket.includes(action.payload)) {
        newState = state.basket.filter(item => item !== action.payload)
      } else {
        newState = state.basket.concat(action.payload);
      }

      state.basket = newState;
      setCookie(null, 'basket', JSON.stringify(newState), {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      });
    }
  },
});

export const {
  setBasket,
  addItemToBasket
} = basketSlice.actions;

export default configureStore({
  reducer: {
    basket: basketSlice.reducer,
  },
})