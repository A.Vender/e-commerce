export const API_URL: string = "http://localhost:1337";
export const BASKET_BUTTON_NAME: string = "Basket";
export const BASKET_URL: string = "/basket";
export const CAROUSEL_ITEM_STEP: number = 2;
export const CAROUSEL_TRANSFORM_STEP: number = 50;


interface AuthNavigationTypes {
  /** Page url */
  url: string;
  /** Page name */
  name: string;
}
export const AUTH_NAVIGATION: Array<AuthNavigationTypes> = [{
  url: '/',
  name: 'All products'
}];